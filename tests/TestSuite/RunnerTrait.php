<?php

namespace App\Test\TestSuite;

use PHPUnit\Framework\MockObject\MockObject;
use App\Model\Table\ServicesTable;
use App\Test\TestSuite\MockRunner;

/**
 * Wrap the Runners with mock objects
 * 
 * @implements App\Test\TestSuite\ControllerTestCase
 */
trait RunnerTrait
{
    
    /**
     * Mock of the Services table
     *
     * @var MockObject|ServicesTable
     */
    protected $services_mock = null;
    
    /**
     * Mock Runner instance, returned by the mock services object
     * 
     * @var MockRunner 
     */
    protected $runner_mock = null;

    /**
     * Set RunnerFactory to return mock runners
     * 
     * @before
     * @return void
     */
    public function setupRunners(): void
    {
        $this->runner_mock = new MockRunner();
        
        $this->services_mock = $this->getMockForModel('Services', ['createRunner']);
        $this->services_mock->method('createRunner')
                ->will($this->returnValue($this->runner_mock));
    }

    /**
     * Undo mock runner return
     * 
     * @after
     * @return void
     */
    public function cleanupRunners(): void
    {
        $this->getTableLocator()->clear();
    }

}
