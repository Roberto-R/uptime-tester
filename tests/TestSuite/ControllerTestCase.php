<?php

namespace App\Test\TestSuite;

use Cake\TestSuite\TestCase;
use Cake\TestSuite\IntegrationTestTrait;
use App\Test\TestSuite\Tester\ActionTester;
use App\Test\TestSuite\Tester\FormActionTester;
use Cake\ORM\TableRegistry;

/**
 * Base TestCase for controllers
 *
 * @property \App\Model\Table\UsersTable $Users
 */
abstract class ControllerTestCase extends TestCase
{

    use IntegrationTestTrait;

    private $auth_user_id = null;
    protected $Users;

    /**
     * Camel-cased controller name, also the model alias
     *
     * @var string
     */
    protected $alias = null;

    /**
     * Lower-case controller name, to be used in URL building
     *
     * @var string
     */
    protected $controller = null;

    /**
     * Run before each test
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->enableCsrfToken();
        $this->enableSecurityToken();

        if (empty($this->controller))
        {
            $class_names = namespaceSplit(static::class);
            $this->alias = substr(end($class_names), 0, -14);
            $this->controller = strtolower($this->alias);
        }

        $this->Users = TableRegistry::getTableLocator()->get('Users');
    }

    /**
     * Run after each test
     */
    public function tearDown(): void
    {
        parent::tearDown();

        $this->auth_user_id = null;
    }

    /**
     * Get literal response as string
     *
     * @param int $length
     * @return string
     */
    protected function getResponseSubstring($length = 300)
    {
        $text = (string) $this->_response->getBody();
        return substr($text, 0, $length);
    }

    /**
     * Get decoded JSON from the response
     *
     * @return array
     */
    protected function getResponseJson() : array
    {
        $text = (string) $this->_response->getBody();
        return json_decode($text, true);
    }

    /**
     * Get model alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Create session for a specific user
     *
     * Leave $id null to clear the user.
     *
     * @param int $id
     */
    protected function setAuthUser($id = null)
    {
        $this->auth_user_id = $id;

        if ($id)
        {
            $user = $this->Users->get($id, ['finder' => 'auth']);
            $this->session(['Auth' => $user]);
        }
        else
        {
            $this->session(['Auth' => []]);
        }
    }

    /**
     * Assert response was 403-unauthorized
     *
     * Note: the HTTP code is actually probably a redirect
     */
    public function assertResponseUnauthorized()
    {
        $this->assertResponseCode(403);
    }

    /**
     * Basic test for /index
     */
    public function basicIndex()
    {
        $tester = new ActionTester($this);

        return $tester->setUrl($this->controller . "/index");
    }

    /**
     * Basic test for /view/x
     *
     * @param int $id
     */
    public function basicView($id)
    {
        $tester = new ActionTester($this);

        return $tester->setUrl("{$this->controller}/view/{$id}");
    }

    /**
     * Basic test for /add (or /edit/null)
     */
    public function basicAdd()
    {
        $tester = new FormActionTester($this);

        return $tester
                ->setUrl("{$this->controller}/add")
                ->setAssertBeforeData(null); // Must not yet be present
    }

    /**
     * Basic test for /edit/x
     *
     * @param int $id
     */
    public function basicEdit($id)
    {
        $tester = new FormActionTester($this);

        return $tester
                ->setUrl("/{$this->controller}/edit/{$id}")
                ->setAssertBeforeData(['id' => $id]);
    }

    /**
     * Basic test for /delete/x
     *
     * @param int $id
     */
    public function basicDelete($id)
    {
        $tester = new FormActionTester($this);

        return $tester
                ->setUrlGet(false)
                ->setUrlPost("/{$this->controller}/delete/{$id}")
                ->setAssertBeforeData(['id' => $id])
                ->setAssertAfterData(null); // No longer present
    }

}
