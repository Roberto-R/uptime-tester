<?php

namespace App\Test\TestSuite\Tester;

use App\Test\TestSuite\ControllerTestCase;
use Cake\ORM\TableRegistry;

/**
 * Class to create standard tests
 * 
 * Many controller actions (pages) require similar tests. Use objects like this
 * to create those standard tests with little code.
 */
class ActionTester
{

    /**
     * The parenting test case, needed for all actions and asserts
     * 
     * @var ControllerTestCase $test_case
     */
    protected $test_case = null;
    
    /**
     * Model corresponding to controller
     * 
     * @var \Cake\ORM\Table
     */
    protected $model = null;

    /**
     * URL to call
     * 
     * @var string
     */
    protected $url = null;

    /**
     * Constructor
     * 
     * Pass the parenting test-case to access test functionality.
     * 
     * @param ControllerTestCase $test_case
     */
    public function __construct(ControllerTestCase $test_case)
    {
        $this->test_case = $test_case;
        
        $this->model = TableRegistry::getTableLocator()->get($this->test_case->getAlias());
    }

    /**
     * Set URL to test with
     * 
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
    
    /**
     * Actually execute the actions and assertions
     * 
     * Perform a single get request
     */
    public function run()
    {
        $this->test_case->get($this->url);
        $this->test_case->assertResponseOk();
    }

}
