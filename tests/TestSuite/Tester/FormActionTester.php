<?php

namespace App\Test\TestSuite\Tester;

/**
 * Test form actions
 * 
 * These tests consist of a get request, followed by a post request.
 * And the database content before and after will be compared.
 */
class FormActionTester extends ActionTester
{

    /**
     * URL of the first GET request
     * 
     * Set to `false` to skip
     * 
     * @var string
     */
    protected $url_get = null;

    /**
     * URL of the POST request
     * 
     * Set to `false` to skip
     * 
     * @var string
     */
    protected $url_post = null;

    /**
     * Data to post to $url
     * 
     * @var array
     */
    protected $post_data = [];

    /**
     * Data to assert on entity after save
     * 
     * When `true`, use $post_data instead.
     * When `false`, skip after-save assert.
     * When `null`, assert that $assert_before_data no longer exists.
     * 
     * @var array
     */
    protected $assert_after_data = true;

    /**
     * Data to assert on entity before save
     * 
     * When `false`, skip pre-save assert.
     * When `null`, assert that $assert_after_data does not yet exist.
     * 
     * @var array
     */
    protected $assert_before_data = false;
    
    /**
     * Check redirect after post
     * 
     * When `null`, check for any redirect.
     * When `false`, skip redirect check.
     * Otherwise, verify this is the redirect target.
     * 
     * @var type
     */
    protected $redirect_url = null;

    /**
     * Set data to be posted to page
     * 
     * @param type $data
     */
    public function setPostData($data)
    {
        $this->post_data = $data;
        return $this;
    }

    /**
     * Set post assertion data
     * 
     * @see $this->assert_after_data
     * @param mixed $data
     * @return $this
     */
    public function setAssertAfterData($data)
    {
        $this->assert_after_data = $data;
        return $this;
    }

    /**
     * Set get assertion data
     * 
     * @see $this->assert_before_data
     * @param mixed $data
     * @return $this
     */
    public function setAssertBeforeData($data)
    {
        $this->assert_before_data = $data;
        return $this;
    }

    /**
     * 
     * @return array
     * @see $this->assert_after_data
     */
    public function getAssertAfterData()
    {
        if (is_null($this->assert_after_data))
        {
            return $this->getAssertBeforeData();
        }

        if ($this->assert_after_data === true)
        {
            return $this->post_data;
        }

        return $this->assert_after_data;
    }

    /**
     * 
     * @return array
     * @see $this->assert_before_data
     */
    public function getAssertBeforeData()
    {
        if (is_null($this->assert_before_data))
        {
            return $this->getAssertAfterData();
        }

        return $this->assert_before_data;
    }

    /**
     * Set GET URL
     * 
     * @param string $url
     * @return $this
     */
    public function setUrlGet($url)
    {
        $this->url_get = $url;
        return $this;
    }

    /**
     * Set POST URL
     * 
     * @param string $url
     * @return $this
     */
    public function setUrlPost($url)
    {
        $this->url_post = $url;
        return $this;
    }
    
    /**
     * Set redirect URL (for assert purposes)
     * 
     * @param string $url
     * @return $this
     */
    public function setRedirectUrl($url)
    {
        $this->redirect_url = $url;
        return $this;
    }

    /**
     * Set both POST and GET URL
     * 
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        return $this->setUrlGet($url)->setUrlPost($url);
    }

    /**
     * Test execution
     */
    public function run()
    {
        if ($this->getAssertBeforeData())
        {
            $where = $this->getAssertBeforeData();

            $entity = $this->model->find()
                    ->where($where)
                    ->first();

            if (is_null($this->assert_before_data))
            {
                $this->test_case->assertNull(
                        $entity,
                        'Failed to assert data was not already present'
                );
            }
            else
            {
                $this->test_case->assertNotNull(
                        $entity,
                        'Failed to assert entity data before action'
                );
            }
        }

        // Get to form
        if ($this->url_get)
        {
            $this->test_case->get($this->url_get);
            $this->test_case->assertResponseOk();
        }

        // Post to form
        if ($this->url_post)
        {
            $this->test_case->post($this->url_post, $this->post_data);
            
            if ($this->redirect_url !== false)
            {
                $this->test_case->assertRedirect($this->redirect_url);
            }
        }


        if ($this->getAssertAfterData())
        {
            $where = $this->getAssertAfterData();

            $entity = $this->model->find()
                    ->where($where)
                    ->first();

            if (is_null($this->assert_after_data))
            {
                $this->test_case->assertNull(
                        $entity,
                        'Failed to assert entity no longer exists'
                );
            }
            else
            {
                $this->test_case->assertNotNull(
                        $entity,
                        'Failed to assert entity has been modified'
                );
            }
        }
    }

}
