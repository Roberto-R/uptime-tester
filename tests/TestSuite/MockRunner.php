<?php

namespace App\Test\TestSuite;

use App\Runner\Runner;
use App\Model\Entity\Service;

/**
 * Mock runner, will always succeed or fail based on predetermined input
 */
class MockRunner extends Runner
{

    protected $success = true;

    /**
     * Override parent constructor - No actual input is needed for mock
     */
    public function __construct($service = null)
    {
        if (is_null($service))
        {
            $service = new Service([
                'host_id' => 1,
                'name' => 'Mock Service',
                'type' => 'mock',
                'target' => 'example.com',
                'rate' => 0
            ]);
        }
        
        parent::__construct($service);
    }

    /**
     * Set return status of this mock runner
     * 
     * @param bool $success
     */
    public function setSuccess(bool $success)
    {
        $this->success = $success;
    }

    /**
     * Set return respone of this mock runner
     * 
     * @param string $respone
     */
    public function setResponse(string $respone)
    {
        $this->response = $respone;
    }

    /**
     * Actual runner execution
     * 
     * @return bool
     */
    protected function _run(): bool
    {
        return $this->success; // Don't do anything else
    }

}
