<?php

declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PingsFixture
 */
class PingsFixture extends TestFixture
{

    /**
     * Import existing table (which in turn is based on Migrations)
     *
     * @var array
     */
    public $import = ['table' => 'pings'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'service_id' => 1, // Roberts HTTP
                'success' => 1,
                'started' => '2020-11-07 12:00:00',
                'finished' => '2020-11-07 12:00:00',
                'response' => '#1 This is the ping response.',
                'created' => '2020-11-07 12:27:21',
            ],
            2 => [
                'service_id' => 1, // Roberts HTTP
                'success' => 0,
                'started' => '2020-11-08 12:00:00',
                'finished' => '2020-11-08 12:00:00',
                'response' => '#2 This ping has failed!',
                'created' => '2020-12-25 17:00:00',
            ],
            3 => [
                'service_id' => 1, // Roberts HTTP
                'success' => 1,
                'started' => '2020-11-09 12:00:00',
                'finished' => '2020-11-09 12:00:00',
                'response' => '#3 This is the ping response.',
                'created' => '2020-12-25 17:00:00',
            ],
            4 => [
                'service_id' => 2, // Roberts HTTP
                'success' => 1,
                'started' => '2020-11-09 10:00:00',
                'finished' => '2020-11-09 10:00:00',
                'response' => '#4 This is the ping response.',
                'created' => '2020-12-25 17:00:00',
            ],
        ];
        parent::init();
    }

}
