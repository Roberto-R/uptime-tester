<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * EmailsFixture
 */
class EmailsFixture extends TestFixture
{

    /**
     * Import existing table (which in turn is based on Migrations)
     *
     * @var array
     */
    public $import = ['table' => 'emails'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'service_id' => 1,
                'email' => 'test.test@example.com',
                'created' => '2021-02-27 16:23:38',
                'modified' => '2021-02-27 16:23:38',
            ],
        ];
        parent::init();
    }
}
