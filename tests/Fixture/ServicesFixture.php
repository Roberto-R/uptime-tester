<?php

declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use App\Model\Table\ServicesTable;

/**
 * ServicesFixture
 */
class ServicesFixture extends TestFixture
{

    /**
     * Import existing table (which in turn is based on Migrations)
     *
     * @var array
     */
    public $import = ['table' => 'services'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'host_id' => 1,
                'name' => 'HTTP service for Roberts server',
                'active' => 1,
                'type' => ServicesTable::TYPE_HTTP,
                'target' => 'https://roberts-website.nl/',
                'rate' => 60, // Every hour
                'created' => '2020-11-01 09:51:26',
                'modified' => '2020-11-01 09:51:26',
            ],
            2 => [
                'host_id' => 1,
                'name' => 'Ping service for Roberts server',
                'active' => 1,
                'type' => ServicesTable::TYPE_PING,
                'target' => 'https://roberts-website.nl/',
                'rate' => 360, // Every 6 hours
                'created' => '2020-12-25 17:00:00',
                'modified' => '2020-12-25 17:00:00',
            ],
        ];
        parent::init();
    }

}
