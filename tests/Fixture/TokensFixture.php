<?php

declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use Cake\I18n\FrozenTime;

/**
 * TokensFixture
 */
class TokensFixture extends TestFixture
{

    /**
     * Import existing table (which in turn is based on Migrations)
     * 
     * @var array
     */
    public $import = ['table' => 'tokens'];

    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'user_id' => 1,
                'identifier' => 'czzbjxwoxtetehfertio',
                'token' => '$2y$12$4TNuinXEZ9T.TCaHIF0ktuBZS4PUceHjr2cAMQRnaDaFZYOgitBUe', // "omoinerbimin"
                'created' => new FrozenTime('-1 hour'),
            ],
            2 => [
                'user_id' => 3,
                'identifier' => 'kmwnwmobminivninrinrein',
                'token' => '$2y$12$zvztCdfeqDxZ0tlQb6uTt.i5gx6mcFXr/LnTJ7okdTfiD5uVw7BO.', // "rmknbirnbinbintrtn"
                'created' => new FrozenTime('-1 hour'),
            ],
        ];
        parent::init();
    }

}
