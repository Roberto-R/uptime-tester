<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;
use App\Model\Table\UsersTable;

/**
 * UsersFixture
 */
class UsersFixture extends TestFixture
{
    /**
     * Import existing table (which in turn is based on Migrations)
     * 
     * @var array
     */
    public $import = ['table' => 'users'];
    
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            1 => [
                'email' => 'robert.soor@gmail.com',
                'name' => 'Robert',
                'password' => '$2y$12$E4YjsPkiz33s6KgeFwZf9u.mUZe7r3AfwfdAJIg20Od36loCCGiVe', // "wachtwoord"
                'type' => UsersTable::TYPE_USER,
                'created' => '2020-11-21 22:22:56',
                'modified' => '2020-11-21 22:22:56'
            ],
            2 => [
                'email' => 'admin@gmail.com',
                'name' => 'Admin',
                'password' => '$2y$12$E4YjsPkiz33s6KgeFwZf9u.mUZe7r3AfwfdAJIg20Od36loCCGiVe', // "wachtwoord"
                'type' => UsersTable::TYPE_ADMIN,
                'created' => '2020-12-13 16:00:00',
                'modified' => '2020-12-13 16:00:00'
            ],
            3 => [
                'email' => 'new@gmail.com',
                'name' => 'Unactivated Account',
                'password' => '$2y$12$E4YjsPkiz33s6KgeFwZf9u.mUZe7r3AfwfdAJIg20Od36loCCGiVe', // "wachtwoord"
                'type' => null,
                'created' => '2020-12-19 14:00:00',
                'modified' => '2020-12-19 14:00:00'
            ],
        ];
        parent::init();
    }
}
