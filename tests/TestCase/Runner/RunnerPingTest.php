<?php

use Cake\TestSuite\TestCase;
use App\Model\Table\ServicesTable;
use Cake\ORM\TableRegistry;

/**
 * Test Ping Runner
 * 
 * These test perform actual remote connections. So the chances are good
 * these tests are better skipped!
 * 
 * @property ServicesTable $Services
 * @property App\Model\Entity\Service $service
 */
class RunnerPingTest extends TestCase
{

    public $fixtures = [
        'app.Services'
    ];

    /**
     * setUp
     * 
     * @return void
     */
    public function setUp(): void
    {
        $this->Services = TableRegistry::getTableLocator()->get('Services');

        $this->service = $this->Services->newEntity([
            'target' => 'example.com',
            'type' => ServicesTable::TYPE_PING,
            'name' => 'MyPingService',
            'rate' => null
        ]);

        parent::setUp();
    }

    /**
     * Test regular execution
     */
    public function testRun(): void
    {
        $runner = $this->Services->createRunner($this->service);

        $result = $runner->run();
        
        $this->assertTrue($result);
    }

}
