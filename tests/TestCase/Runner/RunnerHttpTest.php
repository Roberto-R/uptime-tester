<?php

use Cake\TestSuite\TestCase;
use App\Model\Table\ServicesTable;
use Cake\ORM\TableRegistry;

/**
 * Test HTTP Runner
 * 
 * These test perform actual remote connections. So the chances are good
 * these tests are better skipped!
 * 
 * @property ServicesTable $Services
 * @property App\Model\Entity\Service $service
 */
class RunnerHttpTest extends TestCase
{

    public $fixtures = [
        'app.Services'
    ];

    /**
     * setUp
     * 
     * @return void
     */
    public function setUp(): void
    {
        $this->Services = TableRegistry::getTableLocator()->get('Services');

        $this->service = $this->Services->newEntity([
            'target' => 'https://example.com',
            'type' => ServicesTable::TYPE_HTTP,
            'name' => 'MyHTTPService',
            'rate' => null
        ]);

        parent::setUp();
    }

    /**
     * Test regular execution
     */
    public function testRun(): void
    {
        $runner = $this->Services->createRunner($this->service);

        $result = $runner->run();
        
        $this->assertTrue($result);
    }

}
