<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Controller\EmailsController;
use App\Model\Table\EmailsTable;
use App\Test\TestSuite\ControllerTestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\EmailsController Test Case
 *
 * @uses \App\Controller\EmailsController
 * @property EmailsTable $Emails
 */
class EmailsControllerTest extends ControllerTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Emails',
        'app.Services',
        'app.Hosts',
        'app.Users'
    ];

    /**
     * Before each test
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->setAuthUser(1);

        $this->Emails = TableRegistry::getTableLocator()->get('Emails');
    }

    /**
     * Test add method
     */
    public function testAdd(): void
    {
        $this->basicAdd()
            ->setUrl("emails/add/1")
            ->setPostData(['email' => 'newemail@example.org'])
            ->run();
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete(): void
    {
        $this->basicDelete(1)
            ->run();
    }
}
