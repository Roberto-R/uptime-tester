<?php

declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestSuite\ControllerTestCase;
use App\Controller\UsersController;
use Cake\TestSuite\EmailTrait;
use App\Model\Table\UsersTable;
use Cake\Auth\DefaultPasswordHasher;

/**
 * App\Controller\UsersController Test Case
 *
 * @uses \App\Controller\UsersController
 */
class UsersControllerTest extends ControllerTestCase
{

    use EmailTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Users',
        'app.Tokens',
        'app.Hosts',
        'app.Services',
        'app.Pings',
        'app.Emails'
    ];

    /**
     * Test login page
     */
    public function testLogin(): void
    {
        $this->get("/users/login");
        $this->assertResponseOk();

        $this->assertSessionNotHasKey('Auth.id');

        $data = ['email' => 'robert.soor@gmail.com', 'password' => 'wachtwoord'];

        $this->post("/users/login", $data);

        $this->assertRedirect();
        $this->assertSession(1, 'Auth.id'); // Assert session was craeted
    }

    /**
     * Test login page with wrong credentials
     */
    public function testLoginIncorrect(): void
    {
        $data = ['email' => 'robert.soor@gmail.com', 'password' => 'incorrect'];

        $this->post("/users/login", $data);

        $this->assertResponseOk();
        $this->assertSessionNotHasKey('Auth.id');
    }

    /**
     * Test login when already logged in
     */
    public function testLoginRedirect(): void
    {
        $this->setAuthUser(1);
        $this->get("/users/login");
        $this->assertRedirect();
    }

    /**
     * Test logout method
     */
    public function testLogout(): void
    {
        $this->setAuthUser(1);
        $this->get("/users/logout");
        $this->assertRedirect();
        $this->assertSessionNotHasKey('Auth.id');
    }

    /**
     * Test profile method
     */
    public function testProfile(): void
    {
        $this->setAuthUser(1);
        $this->basicEdit(1)
                ->setPostData(['name' => 'Very Cool New Name'])
                ->setUrl("/users/profile")
                ->setRedirectUrl(false)
                ->run();
    }

    /**
     * Test delete method for your own account
     */
    public function testDeleteSelf(): void
    {
        $this->setAuthUser(1);
        $this->basicDelete(1)
                ->setUrlPost("/users/delete-self")
                ->run();

        $this->setAuthUser(false);

        $this->post("/users/login", [
            'email' => 'robert.soor@gmail.com',
            'password' => 'wachtwoord'
        ]);
        $this->assertResponseOk(); // No redirect
    }

    /**
     * Test delete method for your own account
     */
    public function testDelete(): void
    {
        $this->setAuthUser(2); // Admin
        $this->basicDelete(3)
            ->run();
    }

    /**
     * Test register method
     */
    public function testRegister(): void
    {
        $this->get("/users/register");

        $data = [
            'email' => 'bob@example.com',
            'name' => 'Bob',
            'password' => 'iamastrongpassword',
            'password_confirm' => 'iamastrongpassword'
        ];
        $this->post("/users/register", $data);
        $this->assertRedirect();
        $this->assertMailCount(1);
        $this->assertMailContains('/users/activate/');
    }

    /**
     * Test activate method
     */
    public function testActivate(): void
    {
        $user_before = $this->Users->get(3);
        $this->assertNull($user_before->type);

        $identifier = 'kmwnwmobminivninrinrein';
        $secret = 'rmknbirnbinbintrtn';
        $this->get("/users/activate/{$identifier}/{$secret}");
        $this->assertRedirect();

        $user = $this->Users->get(3);
        $this->assertEquals(UsersTable::TYPE_USER, $user->type);
    }

    /**
     * Test forgotPassword form page
     */
    public function testForgotPassword(): void
    {
        $this->get("/users/forgot-password");
        $this->assertResponseOk();

        $data = [
            'email' => 'robert.soor@gmail.com',
        ];
        $this->post("/users/forgot-password", $data);
        $this->assertResponseOk();
        $this->assertMailCount(1);
        $this->assertMailContains('/users/recover/');
    }

    /**
     * Test recover form page
     */
    public function testRecover(): void
    {
        $identifier = 'czzbjxwoxtetehfertio';
        $secret = 'omoinerbimin';
        $url = "users/recover/{$identifier}/{$secret}";
        $this->get($url);
        $this->assertResponseOk();

        $data = [
            'password' => 'very_cool_new_password',
            'password_confirm' => 'very_cool_new_password',
        ];
        $this->post($url, $data);
        $this->assertRedirect();

        $user = $this->Users->get(1);
        $hasher = new DefaultPasswordHasher();
        $this->assertTrue($hasher->check($data['password'], $user->password));

    }

    /**
     * Test access to users/index as non-admin
     */
    public function testIndexUnauthorized(): void
    {
        $this->setAuthUser(1);

        $this->get("/users/index");
        $this->assertResponseUnauthorized();
    }

    /**
     * Test access to users/index
     */
    public function testIndex(): void
    {
        $this->setAuthUser(2);

        $this->get("/users/index");
        $this->assertResponseOk();
    }

}
