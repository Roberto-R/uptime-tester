<?php
declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestSuite\ControllerTestCase;
use App\Controller\HostsController;

/**
 * App\Controller\HostsController Test Case
 *
 * @uses \App\Controller\HostsController
 */
class HostsControllerTest extends ControllerTestCase
{
    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Users',
        'app.Hosts',
        'app.Services',
        'app.Pings',
        'app.Emails'
    ];

    public function setUp(): void
    {
        parent::setUp();

        $this->setAuthUser(1); // Admin
    }

    /**
     * Test index method
     */
    public function testIndex(): void
    {
        $this->basicIndex()->run();
    }

    /**
     * Test edit method (new entity)
     */
    public function testAdd(): void
    {
        $this->basicAdd()
                ->setUrl('hosts/edit')
                ->setPostData([
                    'name' => 'Newly added host',
                    'domain' => 'my-host.com'
                ])
                ->run();
    }

    /**
     * Test edit method (with existing method)
     */
    public function testEdit(): void
    {
        $this->basicEdit(1)
                ->setPostData(['name' => 'Roberts Host - edited!'])
                ->run();
    }

    /**
     * Test view method
     */
    public function testView(): void
    {
        $this->basicView(1)->run();
    }

    /**
     * Test delete method
     */
    public function testDelete(): void
    {
        $this->basicDelete(1)->run();
    }

}
