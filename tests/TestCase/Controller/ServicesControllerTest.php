<?php

declare(strict_types=1);

namespace App\Test\TestCase\Controller;

use App\Test\TestSuite\ControllerTestCase;
use App\Controller\ServicesController;
use Cake\ORM\TableRegistry;
use App\Model\Table\ServicesTable;
use App\Test\TestSuite\RunnerTrait;
use Cake\I18n\FrozenTime;

/**
 * App\Controller\ServicesController Test Case
 *
 * @uses \App\Controller\ServicesController
 * @property ServicesTable $Services
 */
class ServicesControllerTest extends ControllerTestCase
{

    use RunnerTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Users',
        'app.Services',
        'app.Emails',
        'app.Hosts',
        'app.Pings',
    ];

    /**
     * Before each test
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->setAuthUser(1);

        $this->Services = TableRegistry::getTableLocator()->get('Services');
    }

    /**
     * Test view method
     */
    public function testView(): void
    {
        $this->basicView(1)->run();

        $this->assertResponseContains('test.test@example.com');
    }

    /**
     * Test view method with filter options
     */
    public function testViewFilter(): void
    {
        $this->get("/services/view/1?show_success=1&show_failed=0");
        $this->assertResponseNotContains('2020-11-08 12:00');
        $this->assertResponseOk();
        $this->get("/services/view/1?show_success=0&show_failed=1");
        $this->assertResponseNotContains('2020-11-07 12:00');
        $this->assertResponseOk();
        $this->get("/services/view/1?show_success=0&show_failed=0");
        $this->assertResponseOk();
        $this->assertResponseNotContains('2020-11-07 12:00');
        $this->assertResponseNotContains('2020-11-08 12:00');
    }

    /**
     * Test edit method (when adding)
     */
    public function testAdd(): void
    {
        $this->basicAdd()
                ->setUrl("services/edit?host_id=1")
                ->setPostData([
                    'host_id' => 1,
                    'name' => 'Roberts new service!',
                    'type' => ServicesTable::TYPE_HTTP,
                    'target' => 'https://google.com'
                ])
                ->run();
    }

    /**
     * Test edit method (when modifying)
     */
    public function testEdit(): void
    {
        $this->basicEdit(1)
                ->setPostData([
                    'host_id' => 1,
                    'name' => 'Edited server!',
                    'target' => 'https://new-url.com'
                ])
                ->run();
    }

    /**
     * Test delete method
     */
    public function testDelete(): void
    {
        $this->basicDelete(1)->run();
    }

    /**
     * Test the services test method
     */
    public function testTest(): void
    {
        $id = 1;

        foreach ([true, false] as $success)
        {
            $this->runner_mock->setSuccess($success);

            $this->get("/services/test/{$id}.json");
            $this->assertResponseOk();

            $response = $this->getResponseJson();
            $this->assertArrayHasKey('success', $response);
            $this->assertEquals($success, $response['success']);
        }
    }

    /**
     * Test the ping method
     */
    public function testPing(): void
    {
        $id = 1;

        $this->post("/services/ping/{$id}.json");
        $this->assertResponseOk();

        $response = $this->getResponseJson();
        $this->assertArrayHasKey('success', $response);
        $this->assertTrue($response['success']);

        $new_ping_count = $this->Services->Pings->find()
                ->where(['service_id' => $id])
                ->andWhere(['created >=' => new FrozenTime('-5 seconds')])
                ->count();

        $this->assertEquals(1, $new_ping_count);
    }

}
