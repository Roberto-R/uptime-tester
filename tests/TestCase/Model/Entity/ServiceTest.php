<?php

use Cake\TestSuite\TestCase;
use App\Model\Entity\Service;
use App\Model\Table\ServicesTable;
use Cake\ORM\TableRegistry;

/**
 * Test case for the Service entity
 * 
 * @property Service $service
 * @property ServicesTable $Services
 */
class ServiceTest extends TestCase
{

    protected $service = null;
    public $fixtures = [
        'app.Services'
    ];

    /**
     * Prepare test
     * 
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->Services = TableRegistry::getTableLocator()->get('Services');
        $this->service = $this->Services->newEntity([
            'target' => 'example.com',
            'type' => ServicesTable::TYPE_PING,
            'name' => 'MyPingService',
            'rate' => null
        ]);
    }
    
    /**
     * Clean up after test
     * 
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        
        unset($this->service);
        unset($this->Services);
    }

    /**
     * Test virtual field
     */
    public function testUptimeFromTotal(): void
    {
        $service = $this->service;
        
        $service->time_total = null;
        $service->uptime = null;
        $this->assertNull($service->uptimeFromTotal);
        
        $service->time_total = 0.1;
        $service->uptime = 0.1;
        $this->assertNull($service->uptimeFromTotal);
        
        $service->time_total = 10.0;
        $service->uptime = 5.0;
        $this->assertEquals(0.50, $service->uptimeFromTotal);
    }

    /**
     * Test virtual field
     */
    public function testIntervalFormat(): void
    {
        $service = $this->service;
        
        $service->rate = null;
        $this->assertEquals('Never', $service->intervalFormat);
        
        $service->rate = 45;
        $this->assertEquals('45 minutes', $service->intervalFormat);
        
        $service->rate = 720;
        $this->assertEquals('12 hour(s)', $service->intervalFormat);
        
        $service->rate = 4320;
        $this->assertEquals('3 day(s)', $service->intervalFormat);
    }

}
