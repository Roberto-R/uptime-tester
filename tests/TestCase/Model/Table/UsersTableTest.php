<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\TestSuite\TableTestCase;
use App\Model\Table\UsersTable;

/**
 * App\Model\Table\UsersTable Test Case
 * 
 * @property UsersTable $Users
 */
class UsersTableTest extends TableTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Users'
    ];
    
    // Rely on parent class tests
}
