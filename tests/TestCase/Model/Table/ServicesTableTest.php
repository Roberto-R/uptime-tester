<?php

declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Test\TestSuite\TableTestCase;
use App\Model\Table\ServicesTable;

/**
 * App\Model\Table\ServicesTable Test Case
 * 
 * @property ServicesTable $Services
 */
class ServicesTableTest extends TableTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Services',
        'app.Hosts',
        'app.Pings',
    ];

    // Rely on parent class tests
    
    /**
     * Test validation sets tied through build rules
     */
    public function testTypSpecificValidation(): void
    {
        $service_http = $this->Services->newEntity([
            'host_id' => 1,
            'name' => 'New HTTP Service',
            'type' => ServicesTable::TYPE_HTTP,
            'target' => 'invalid@url!'
        ]);
        
        // Dependent on build rules, only called on save
        $this->Services->save($service_http);
        
        $errors_http = $service_http->getError('target');
        $this->assertArrayHasKey('url', $errors_http);
        
        
        $service_ping = $this->Services->newEntity([
            'host_id' => 1,
            'name' => 'New Ping Service',
            'type' => ServicesTable::TYPE_PING,
            'target' => 'http://not-a.valid.domain'
        ]);
        
        // Dependent on build rules, only called on save
        $this->Services->save($service_ping);
        
        $errors_ping = $service_ping->getError('target');
        $this->assertArrayHasKey('regex', $errors_ping);
    }
}
