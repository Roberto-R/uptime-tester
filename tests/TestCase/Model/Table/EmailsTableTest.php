<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EmailsTable;
use App\Test\TestSuite\TableTestCase;

/**
 * App\Model\Table\EmailsTable Test Case
 *
 * @property EmailsTable $Emails
 */
class EmailsTableTest extends TableTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Services',
        'app.Hosts',
        'app.Pings',
        'app.Emails',
    ];

    // Rely on parent class tests
}
