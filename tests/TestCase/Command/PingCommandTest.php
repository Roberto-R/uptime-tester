<?php

declare(strict_types=1);

namespace App\Test\TestCase\Command;

use App\Command\PingCommand;
use Cake\TestSuite\ConsoleIntegrationTestTrait;
use Cake\TestSuite\EmailTrait;
use App\Test\TestSuite\RunnerTrait;
use Cake\TestSuite\TestCase;
use Cake\I18n\FrozenTime;
use Cake\ORM\TableRegistry;

/**
 * App\Command\PingCommand Test Case
 */
class PingCommandTest extends TestCase
{

    use ConsoleIntegrationTestTrait;
    use EmailTrait;
    use RunnerTrait;

    public $fixtures = [
        'app.Users',
        'app.Hosts',
        'app.Services',
        'app.Pings',
        'app.Emails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->useCommandRunner();

        FrozenTime::setTestNow('2020-12-12 16:00:00'); // Put in fake current datetime to make ping
        // time comparisons predictable
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();

        FrozenTime::setTestNow(); // Restore real datetime
    }

    public function testDescriptionOutput()
    {
        $this->exec('ping --help');
        $this->assertOutputContains('ping all services');
    }

    /**
     * Test execute method
     */
    public function testExecute(): void
    {
        $this->exec('ping');
        $this->assertOutputContains('Pinged 2 services');
    }

    /**
     * Test execute method when runners fail
     */
    public function testExecuteFail(): void
    {
        $this->runner_mock->setSuccess(false);
        $this->exec('ping');
        $this->assertOutputContains('Pinged 2 services');
        $this->assertMailCount(3); // Two users and an extra email
    }

    /**
     * Test execute method when an error occurred
     */
    public function testExecuteError(): void
    {
        // Mock the Pings table
        $pings_mock = $this->getMockForModel('Pings', ['save']);
        $pings_mock->method('save')->will($this->returnValue(false));

        $this->exec('ping');
        $this->assertErrorContains('Failed to save');
    }

}
