<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

/**
 * Adds the `Emails` model, allowing for additional emails to be sent per service
 */
class AddEmails extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * @return void
     */
    public function change()
    {
        $table = $this->table('emails');
        $table
            ->addColumn('service_id', 'integer', [
                'null' => false
            ])
            ->addColumn('email', 'string', [
                'limit' => 100,
                'null' => false
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'null' => true,
            ]);
        $table->addIndex(['service_id']);
        $table->addForeignKey('service_id', 'services','id', [
                'update' => 'RESTRICT',
                'delete' => 'RESTRICT',
            ]);

        $table->save();
    }
}
