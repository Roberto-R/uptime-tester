<?php

declare(strict_types=1);

use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{

    /**
     * Up Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-up-method
     * @return void
     */
    public function up()
    {
        $this->table('hosts')
                ->addColumn('user_id', 'integer', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ])
                ->addColumn('name', 'string', [
                    'default' => null,
                    'limit' => 100,
                    'null' => false,
                ])
                ->addColumn('description', 'text', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('domain', 'string', [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('modified', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addIndex(
                        [
                            'user_id',
                        ]
                )
                ->create();

        $this->table('pings')
                ->addColumn('service_id', 'integer', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ])
                ->addColumn('success', 'boolean', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ])
                ->addColumn('started', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ])
                ->addColumn('finished', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ])
                ->addColumn('response', 'text', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addIndex(
                        [
                            'service_id',
                        ]
                )
                ->create();

        $this->table('services')
                ->addColumn('host_id', 'integer', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ])
                ->addColumn('name', 'string', [
                    'default' => null,
                    'limit' => 100,
                    'null' => false,
                ])
                ->addColumn('type', 'string', [
                    'default' => null,
                    'limit' => 100,
                    'null' => false,
                ])
                ->addColumn('target', 'string', [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ])
                ->addColumn('rate', 'integer', [
                    'comment' => 'Minutes between automated calls',
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('modified', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addIndex(
                        [
                            'host_id',
                        ]
                )
                ->create();

        $this->table('tokens')
                ->addColumn('user_id', 'integer', [
                    'default' => null,
                    'limit' => null,
                    'null' => false,
                ])
                ->addColumn('identifier', 'string', [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ])
                ->addColumn('token', 'string', [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addIndex(
                        [
                            'user_id',
                        ]
                )
                ->create();

        $this->table('users')
                ->addColumn('email', 'string', [
                    'default' => null,
                    'limit' => 100,
                    'null' => false,
                ])
                ->addColumn('name', 'string', [
                    'default' => null,
                    'limit' => 100,
                    'null' => false,
                ])
                ->addColumn('password', 'string', [
                    'default' => null,
                    'limit' => 255,
                    'null' => false,
                ])
                ->addColumn('type', 'string', [
                    'default' => null,
                    'limit' => 20,
                    'null' => true,
                ])
                ->addColumn('created', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->addColumn('modified', 'datetime', [
                    'default' => null,
                    'limit' => null,
                    'null' => true,
                ])
                ->create();

        $this->table('hosts')
                ->addForeignKey(
                        'user_id',
                        'users',
                        'id',
                        [
                            'update' => 'RESTRICT',
                            'delete' => 'RESTRICT',
                        ]
                )
                ->update();

        $this->table('pings')
                ->addForeignKey(
                        'service_id',
                        'services',
                        'id',
                        [
                            'update' => 'RESTRICT',
                            'delete' => 'RESTRICT',
                        ]
                )
                ->update();

        $this->table('services')
                ->addForeignKey(
                        'host_id',
                        'hosts',
                        'id',
                        [
                            'update' => 'RESTRICT',
                            'delete' => 'RESTRICT',
                        ]
                )
                ->update();

        $this->table('tokens')
                ->addForeignKey(
                        'user_id',
                        'users',
                        'id',
                        [
                            'update' => 'RESTRICT',
                            'delete' => 'RESTRICT',
                        ]
                )
                ->update();
    }

    /**
     * Down Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-down-method
     * @return void
     */
    public function down()
    {
        $this->table('hosts')
                ->dropForeignKey(
                        'user_id'
                )->save();

        $this->table('pings')
                ->dropForeignKey(
                        'service_id'
                )->save();

        $this->table('services')
                ->dropForeignKey(
                        'host_id'
                )->save();

        $this->table('tokens')
                ->dropForeignKey(
                        'user_id'
                )->save();

        $this->table('hosts')->drop()->save();
        $this->table('pings')->drop()->save();
        $this->table('services')->drop()->save();
        $this->table('tokens')->drop()->save();
        $this->table('users')->drop()->save();
    }

}
