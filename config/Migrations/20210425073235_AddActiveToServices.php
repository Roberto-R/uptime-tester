<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

/**
 * Adds the `active` column to the services table.
 */
class AddActiveToServices extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('services');

        $table->addColumn('active', 'boolean', [
            'default' => 1,
            'null' => false,
            'after' => 'name'
        ]);

        $table->update();
    }
}
