<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Email $email
 * @var \App\Model\Entity\Service $service
 */
?>

<div class="form content">
    <?= $this->Form->create($email) ?>
    <fieldset>
        <legend>Add extra email for <i><?= h($service->name) ?></i></legend>
        <?php
        echo $this->Form->control('email');
        ?>
    </fieldset>
    <div class="btn-group" role="group">
        <?= $this->Form->button('Add') ?>
        <?= $this->Html->link('Cancel', ['controller' => 'Services', 'action' => 'view', $service->id], ['class' => 'btn btn-primary']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
