<?php
/* @var $this \App\View\AppView */
/* @var $user App\Model\Entity\User */
?>
<div class="content">

    <h3>Profile</h3>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <?= $this->Form->create($user) ?>
                <fieldset>
                    <legend>Info</legend>
                    <?= $this->Form->control('email') ?>
                    <?= $this->Form->control('name') ?>
                    <?= $this->Form->control('type', ['disabled' => true]) ?>
                </fieldset>
                <?= $this->Form->submit('Save') ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->create($user) ?>
                <fieldset>
                    <legend>Change password</legend>
                    <?= $this->Form->control('password', ['value' => '']) ?>
                    <?= $this->Form->control('password_confirm', ['value' => '', 'type' => 'password']) ?>
                </fieldset>
                <?= $this->Form->submit('Change password') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <br><br>
                <?=
                $this->Form->postLink('Delete your account', ['action' => 'delete-self'], [
                    'class' => 'btn btn-danger',
                    'confirm' => 'Delete your account, including hosts, services and '
                    . 'their uptime information? This cannot be undone!'
                ])
                ?>
            </div>
        </div>
    </div>

</div>
