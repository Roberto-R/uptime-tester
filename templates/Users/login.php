<?php
/* @var $this Cake\View\View */
?>

<div class="form content">
    <?= $this->Form->create() ?>
    <fieldset>
        <legend>Please enter your email and password</legend>
        <?= $this->Form->control('email') ?>
        <?= $this->Form->control('password') ?>
    </fieldset>
    <?= $this->Form->control('remember_me', ['type' => 'checkbox']) ?>
    <?= $this->Form->button('Login') ?>
    <?= $this->Form->end() ?>
    
    <br>
    <?= $this->Html->link('Forgot password', ['action' => 'forgotPassword']) ?>
</div>
