<?php

if ($service->has('last_ping')):

    if ($service->last_ping->success) :
        ?>
        <span class="badge badge-pill badge-success">Online</span>
    <?php else: ?>
        <span class="badge badge-pill badge-warning">Failed</span>
    <?php

    endif;
else :
    ?>

    <span class="badge badge-pill badge-secondary">NaN</span>

<?php endif;
