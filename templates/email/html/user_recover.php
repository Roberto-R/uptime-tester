<?php
/**
 * Email to recover an account
 */
/* @var $user App\Model\Entity\User */
/* @var $token App\Model\Entity\Token */
/* @var $this Cake\View\View */

$url = $this->Url->build([
    'controller' => 'Users',
    'action' => 'recover',
    $token->identifier,
    $token->secret
        ], ['fullBase' => true]);
?>

<p>Dear <?= $user->name ?>,</p>

<p>
    Click on the link below (or copy it into your web browser) to set a new 
    password:
</p>

<p>
    <?= $this->Html->link($url, $url) ?>
</p>

<p>
    If you did not request this email, you need not do anything. Your 
    credentials are still safe.
</p>

<p>
    Kind regards,<br>
    The Uptime-Tester<br>
    (This email has been sent automatically)
</p>
