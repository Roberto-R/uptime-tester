<?php
/**
 * Email to notify the owner of the ping result
 * Works both failures and successes!
 */
/* @var $ping App\Model\Entity\Ping */
/* @var $service App\Model\Entity\Service */
/* @var $this Cake\View\View */
?>

<p>Dear,</p>

<p>
    <?php if ($ping->success) : ?>
        A scheduled ping has finished <b>succesfully</b> for:
    <?php else: ?>
        A scheduled ping has <b>failed</b> for:
    <?php endif; ?>
</p>

<p>
    Service: <b><?= $service->name ?></b>, on the host: <b><?= $service->host->name ?></b>
</p>
<table>
    <tr>
        <th>Type:</th>
        <td><?= $service->typeLabel ?></td>
    </tr>
    <tr>
        <th>Target:</th>
        <td><?= $service->target ?></td>
    </tr>
    <tr>
        <th>Ping started on:</th>
        <td><?= $ping->started ?></td>
    </tr>
</table>

<p>
    Kind regards,<br>
    The Uptime-Tester<br>
    (This email has been sent automatically)
</p>