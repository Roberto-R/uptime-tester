<?php

/* @var $this \Cake\View\View */
?>

<div class="content">
    
    <h1>Help</h1>
    
    <p>
        Welcome to the Uptime Tester! This application allows users to add their
        hosts including services (like HTTP or a simple IP ping) and test them 
        regularly. Test pings are performed are specified intervals, from which
        uptime statistics are computed. Email notifications can be send when a 
        service has failed, letting you know to fix it before anyone else 
        notices!
    </p>
    <p>
        This application has been created by <?= $this->Html->link('Robert Roos', 'https://robert-roos.nl') ?>
    </p>
    
</div>
