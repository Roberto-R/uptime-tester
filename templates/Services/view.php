<?php
/**
 * @var \App\View\AppView         $this
 * @var \App\Model\Entity\Service $service
 */
$this->Html->script('services', ['block' => true]);
?>

<div class="content">
    <h3>Service: <?= h($service->name) ?></h3>
    <table class="table table-sm">
        <tr>
            <th><?= __('Host') ?></th>
            <td><?= $this->Html->link($service->host->name, ['controller' => 'Hosts', 'action' => 'view', $service->host->id]) ?></td>
        </tr>
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($service->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Active') ?></th>
            <td><?= $service->active ? 'Yes' : 'No' ?></td>
        </tr>
        <tr>
            <th><?= __('Type') ?></th>
            <td><?= h($service->typeLabel) ?></td>
        </tr>
        <tr>
            <th><?= __('Target') ?></th>
            <td><?= h($service->target) ?></td>
        </tr>
        <tr>
            <th><?= __('Additional emails') ?></th>
            <td>
                <?php if (!$service->emails): ?>
                    <i>None</i>
                <?php else: ?>
                    <ul>
                        <?php foreach ($service->emails as $email) : ?>
                        <li>
                            <?= h($email->email) ?>&nbsp;
                            <?= $this->Form->postLink('Delete', ['controller' => 'Emails', 'action' => 'delete', $email->id], ['class' => 'btn btn-sm btn-warning p-0']) ?>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <?= $this->Html->link('Add', ['controller' => 'Emails', 'action' => 'add', $service->id], ['class' => 'btn btn-sm btn-info']) ?>
            </td>
        </tr>
    </table>

    <?= $this->Html->link('Back to host', ['controller' => 'Hosts', 'action' => 'view', $service->host_id], ['class' => 'btn btn-secondary']) ?>
    <br><br>
    <?= $this->Html->link('Edit', ['action' => 'edit', $service->id], ['class' => 'btn btn-secondary']) ?>
    <br><br>
    <?= $this->Form->button('Test', ['type' => 'button', 'id' => 'test-service', 'class' => 'btn-primary']) ?>

    <div id="test-result"></div>

    <div class="related">

        <h4><?= __('Pings') ?></h4>


        <?php

        $context = [
            'schema' => [],
            'defaults' => ['show_success' => 1, 'show_failed' => 1]
        ];

        echo $this->Form->create($context, ['type' => 'get', 'valueSources' => ['query', 'context'], 'align' => 'inline', 'id' => 'ping-selection']);
        echo $this->Form->control('show_success', ['type' => 'checkbox', 'label' => 'Successful', 'hiddenField' => false]);
        echo $this->Form->control('show_failed', ['type' => 'checkbox', 'label' => 'Failed', 'hiddenField' => false]);
        echo $this->Form->end();
        ?>
        <br>

        <?php if (!empty($service->pings)) : ?>
            <div class="table-responsive">
                <table class="table table-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('success') ?></th>
                        <th><?=
                            $this->Paginator->sort(
                                'finished', 'Time', ['direction' => 'desc']
                            )
                            ?></th>
                    </tr>
                    <?php foreach ($service->pings as $pings) : ?>
                        <tr>
                            <td>
                                <?php if ($pings->success) : ?>
                                    <span class="badge badge-pill badge-success">Online</span>
                                <?php else: ?>
                                    <span class="badge badge-pill badge-warning">Failed</span>
                                <?php endif; ?>
                            </td>
                            <td><?= h($pings->finished) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        <?php endif; ?>

        <?= $this->element('basics/pagination_panel') ?>

    </div>
</div>

<script>
    $(document).ready(function () {

        $("button#test-service").service({
            id: <?= $service->id ?>,
            dry: true,
            output: "div#test-result"
        });

        $("form#ping-selection input").change(function() {
            var form = $(this).parents("form");

            // Create hidden field for each unchecked checkbox (since an unchecked box sends no data)
            $.each(form.find("input[type='checkbox']"), function (_, checkbox) {
                if (!$(checkbox).is(":checked")) {
                    var name = $(checkbox).prop("name");
                    form.append('<input type="hidden" name="' + name + '" value="0" />');
                }
            });

            form.submit();
        });
    });
</script>
