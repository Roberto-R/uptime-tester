<?php

declare(strict_types=1);

namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use App\Model\Entity\Ping;
use App\Model\Entity\Service;
use Cake\Mailer\Mailer;
use Exception;

/**
 * Ping command
 *
 * Used to ping services regularly.
 * This command should be on a Cron job.
 *
 * @property \App\Model\Table\ServicesTable $Services
 */
class PingCommand extends Command
{

    public $modelClass = 'Services';

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/4/en/console-commands/commands.html#defining-arguments-and-options
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser = parent::buildOptionParser($parser);

        $parser->setDescription('Command to ping all services where a ping is overdue');

        return $parser;
    }

    /**
     * Get all services and perform ping if needed
     *
     * The last ping is compared to a service interval. If the last ping was
     * sufficiently long ago, a new one will run.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|void|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $services = $this->Services->find('neededPings')
                ->select($this->Services->Hosts)
                ->select($this->Services->Hosts->Users) // Needed because of explicit selects above
                ->contain(['Hosts.Users', 'Emails']);

        $count = 0;
        $errors = 0;

        foreach ($services as $service)
        {
            $runner = $this->Services->createRunner($service);

            $success = $runner->run();

            $ping = $this->Services->Pings->savePingResult(
                    $service, $success, $runner
            );

            if (!$success && $ping)
            {
                $this->sendPingEmail($ping, $service);
            }

            if (!$ping)
            {
                $errors += 1;
            }

            $count += 1;
        }

        $io->out(__('Pinged {0} services', $count));
        if ($errors > 0)
        {
            $io->err(__('Failed to save {0} ping results', $errors));
        }
    }

    /**
     * Send ping result by email
     *
     * @param Ping $ping
     * @param Service $service
     */
    protected function sendPingEmail(Ping $ping, Service $service)
    {
        $mailer = new Mailer();
        $name = "'{$service->name}' on '{$service->host->name}'";
        if ($ping->success)
        {
            $subject = "Uptime ping successful for {$name}";
        }
        else
        {
            $subject = "Uptime ping failed for {$name}";
        }
        $mailer->setSubject($subject)
                ->setViewVars(['ping' => $ping, 'service' => $service])
                ->setEmailFormat('html');
        $mailer->viewBuilder()->setTemplate('ping_result');

        // Make list of recipients
        $addresses = [$service->host->user->email];
        if ($service->emails)
        {
            foreach ($service->emails as $email)
            {
                $addresses[] = $email->email;
            }
        }

        foreach ($addresses as $address)
        {
            $mailer->setTo($address);
            $mailer->send(); // Send doesn't seem to throw an error for e.g. an invalid address
        }
    }

}
