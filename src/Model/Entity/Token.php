<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Authentication\PasswordHasher\DefaultPasswordHasher;

/**
 * Token Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $identifier
 * @property string $token
 * @property \Cake\I18n\FrozenTime|null $created
 * @property string $secret Unhashed token (only upon creation!)
 *
 * @property \App\Model\Entity\User $user
 */
class Token extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'identifier' => true,
        'token' => true,
        'created' => true,
        'user' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'token',
    ];

    /**
     * Hash token before setting
     * 
     * @param string $token
     * @return string
     */
    protected function _setToken(string $token)
    {
        $hasher = new DefaultPasswordHasher();
        return $hasher->hash($token);
    }
}
