<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Ping Entity
 *
 * @property int $id
 * @property int $service_id
 * @property bool $success
 * @property \Cake\I18n\FrozenTime $started
 * @property \Cake\I18n\FrozenTime $finished
 * @property string|null $response
 * @property \Cake\I18n\FrozenTime|null $created
 *
 * @property \App\Model\Entity\Service $service
 */
class Ping extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'service_id' => true,
        'success' => true,
        'started' => true,
        'finished' => true,
        'response' => true,
        'created' => true,
        'service' => true,
    ];
}
