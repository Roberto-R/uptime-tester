<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Entity\Ping;
use App\Model\Entity\Service;
use App\Runner\Runner;

/**
 * Pings Model
 *
 * @property \App\Model\Table\ServicesTable&\Cake\ORM\Association\BelongsTo $Services
 *
 * @method \App\Model\Entity\Ping newEmptyEntity()
 * @method \App\Model\Entity\Ping newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Ping[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Ping get($primaryKey, $options = [])
 * @method \App\Model\Entity\Ping findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Ping patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Ping[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Ping|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ping saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Ping[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ping[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ping[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Ping[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('pings');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Services');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->boolean('success')
                ->requirePresence('success', 'create')
                ->notEmptyString('success');

        $validator
                ->dateTime('started')
                ->requirePresence('started', 'create')
                ->notEmptyDateTime('started');

        $validator
                ->dateTime('finished')
                ->requirePresence('finished', 'create')
                ->notEmptyDateTime('finished');

        $validator
                ->scalar('response')
                ->allowEmptyString('response');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['service_id'], 'Services'), ['errorField' => 'service_id']);

        return $rules;
    }

    /**
     * Find the most recent ping
     * 
     * Designed to be used by the LastPings association.
     * The query joins with itself through a subquery. In the subquery the 
     * latest datetime is found for each service. In the outer query the entire 
     * row related to it is selected.
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findMostRecent(Query $query, array $options): Query
    {
        $subquery = $this->find()
                ->select([
                    'sub_pings_service_id' => 'service_id',
                    'sub_pings_max_finished' => 'MAX(finished)'
                ])
                ->group('service_id');

        return $query->innerJoin(
                        ['p' => $subquery],
                        ['sub_pings_service_id = service_id', 'sub_pings_max_finished = finished']
        );
    }

    /**
     * Join Pings with itself to find the next ping, setting the period
     * 
     * Add the `period` column, which is the time in seconds between this ping 
     * and the next. The most recent ping with have `period` equal to NULL.
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findWithPeriod(Query $query, array $options): Query
    {
        $options += ['period' => true];

        if ($options['period'])
        {
            $query->select([
                'period' => 'TIME_TO_SEC(TIMEDIFF(p2.finished, Pings.finished))'
            ]);
        }

        $subquery = 'SELECT MIN(p3.finished) FROM pings p3 WHERE '
                . 'p3.finished > Pings.finished';

        return $query->leftJoin(
                        ['p2' => 'Pings'],
                        "p2.service_id = Pings.service_id AND "
                        . "p2.finished = ({$subquery})"
        );
    }

    /**
     * Create new ping item from 
     * 
     * @param Service|int $service
     * @param bool $success
     * @param Runner $runner
     * @return Ping|false
     */
    public function savePingResult($service, bool $success, Runner $runner)
    {
        $service_id = is_numeric($service) ? $service : $service->id;

        $ping = $this->newEntity([
            'service_id' => $service_id,
            'success' => $success,
            'started' => $runner->getStarted(),
            'finished' => $runner->getFinished(),
            'response' => $runner->getResponse()
        ]);

        if (!$this->save($ping))
        {
            return false;
        }

        return $ping;
    }

}
