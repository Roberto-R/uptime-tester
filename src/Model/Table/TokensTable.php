<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\I18n\Time;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Entity\Token;
use App\Model\Entity\User;
use Cake\Utility\Security;
use Cake\Event\EventInterface;
use ArrayObject;
use Authentication\PasswordHasher\DefaultPasswordHasher;

/**
 * Tokens Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Token newEmptyEntity()
 * @method \App\Model\Entity\Token newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Token[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Token get($primaryKey, $options = [])
 * @method \App\Model\Entity\Token findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Token patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Token[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Token|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Token saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Token[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Token[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Token[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Token[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TokensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('tokens');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->scalar('identifier')
                ->maxLength('identifier', 255)
                ->requirePresence('identifier', 'create')
                ->notEmptyString('identifier');

        $validator
                ->scalar('token')
                ->maxLength('token', 255)
                ->requirePresence('token', 'create')
                ->notEmptyString('token');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'), ['errorField' => 'user_id']);

        // Do not enable rule but instead use beforeSave to enforce it
        //$rules->add($rules->isUnique('user_id', 'Only a single token per user is allowed'));

        return $rules;
    }

    /**
     * Remove existing tokens
     * 
     * @param EventInterface $event
     * @param Token $token
     * @param ArrayObject $options
     */
    public function beforeSave(EventInterface $event, Token $token, ArrayObject $options)
    {
        // Remove existing tokens (if any)
        $this->deleteAll(['user_id' => $token->user_id]);
        // This delete method does _not_ fire events!
    }

    /**
     * Create, save and return token for user
     * 
     * The unhashed token will be put in the 'secret' field.
     * 
     * @param int $user_id
     * @return Token|bool
     */
    public function createAndSaveToken(int $user_id)
    {
        $secret = Security::randomString();

        $token = $this->newEntity([
            'user_id' => $user_id,
            'identifier' => Security::randomString(),
            'token' => $secret
        ]);

        if (!$this->save($token))
        {
            return false; // Something went wrong
        }

        $token->secret = $secret;

        return $token;
    }

    /**
     * Validate a given identifier/secret pair and return corresponding user
     * 
     * Token is included as `$user->token`.
     * 
     * @param string $identifier
     * @param string $secret
     * @return User|false
     */
    public function validate($identifier, $secret)
    {
        $token = $this->find()
                ->where([
                    'identifier' => $identifier,
                    'created >' => new Time('-1 day')
                ])
                ->first();

        if (!$token)
        {
            return false; // Not found
        }

        $hasher = new DefaultPasswordHasher();
        if (!$hasher->check($secret, $token->token))
        {
            return false; // Invalid secret
        }

        $user = $this->Users->get($token->user_id);
        
        $user->token = $token;
        $user->setDirty('token', false);
        
        return $user;
    }

}
