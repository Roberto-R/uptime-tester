<?php

declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property HostsTable&\Cake\ORM\Association\HasMany $Hosts
 * @property TokensTable&\Cake\ORM\Association\HasOne $Tokens
 *
 * @method \App\Model\Entity\User newEmptyEntity()
 * @method \App\Model\Entity\User newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Different user types
     */
    const TYPE_ADMIN = 'admin';
    const TYPE_USER = 'user';

    /**
     * Array of user types
     */
    const TYPES = [
        self::TYPE_ADMIN,
        self::TYPE_USER
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Hosts')
                ->setDependent(true) // Cascading delete
                ->setCascadeCallbacks(true); // Trigger child events

        $this->hasOne('Tokens') // Allow only a single token
                ->setDependent(true); // Cascading delete
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
                ->integer('id')
                ->allowEmptyString('id', null, 'create');

        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->notEmptyString('email');

        $validator
                ->scalar('name')
                ->maxLength('name', 100)
                ->requirePresence('name', 'create')
                ->notEmptyString('name');

        $validator
                ->scalar('password')
                ->minLength('password', 10, 'Passwords must contain at least 10 characters')
                ->maxLength('password', 255)
                ->requirePresence('password', 'create')
                ->notEmptyString('password');

        $validator
                ->notEmptyString('password_confirm')
                ->equalToField('password_confirm', 'password', 'Passwords do not match');

        $validator
                ->scalar('type')
                ->maxLength('type', 20)
                ->allowEmptyString('type')
                ->inList('type', self::TYPES);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['email']), [
            'errorField' => 'email',
            'message' => 'An account with this email already exists'
        ]);

        return $rules;
    }

    /**
     * Get users with their number of owned services
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findCountServices(Query $query, array $options): Query
    {
        // Two levels of aggregation, so join on subquery instead

        $subquery = $this->Hosts->find();
        $subquery->select([
                    'id' => 'Hosts.id',
                    'user_id' => 'Hosts.user_id',
                    'count_services' => 'COUNT(Services.host_id)'
                ])
                ->leftJoinWith('Services')
                ->group('Hosts.id');

        return $query
                        ->select($this)
                        ->select([
                            'count_hosts' => 'COUNT(Hosts.user_id)',
                            'count_services' => 'IFNULL(SUM(Hosts.count_services), 0)'
                        ])
                        ->leftJoin(
                                ['Hosts' => $subquery],
                                ['Users.id = Hosts.user_id']
                        )
                        ->group('Users.id');
    }

    /**
     * Finder used by authenticator
     * 
     * @param Query $query
     * @param array $options
     * @return Query
     */
    public function findAuth(Query $query, array $options): Query
    {
        return $query->where(['Users.type IS NOT NULL']);
    }

}
