<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Services Controller
 *
 * @property \App\Model\Table\ServicesTable $Services
 * @method \App\Model\Entity\Service[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ServicesController extends AppController
{

    /**
     * View method
     *
     * @param string|null $id Service id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $service = $this->Services->get($id, [
            'contain' => ['Hosts', 'Emails'],
        ]);

        $this->Authorization->authorize($service);

        $pings = $this->Services->Pings->find()
                ->where(['Pings.service_id' => $service->id]);

        $show_success = (bool)$this->request->getQuery('show_success', true);
        $show_fail = (bool)$this->request->getQuery('show_failed', true);

        if ($show_success && !$show_fail)
        {
            $pings->where(['Pings.success' => 1]);
        }
        elseif (!$show_success && $show_fail)
        {
            $pings->where(['Pings.success' => 0]);
        }
        elseif (!$show_success && !$show_fail)
        {
            $pings->where(['Pings.success' => 0])
                ->where(['Pings.success' => 1]);
        }

        $this->paginate = [
            'order' => ['Pings.finished' => 'desc']
        ];

        $service->pings = $this->paginate($pings);

        $this->set(compact('service'));
    }

    /**
     * Edit and add method
     *
     * @param string|null $id Service id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (is_null($id))
        {
            $service = $this->Services->newEmptyEntity();
            $this->Authorization->authorize($service, 'create');

            $service->host_id = $this->request->getQuery('host_id');

            if ($service->host_id)
            {
                $host = $this->Services->Hosts->get($service->host_id);
                $service->target = $host->domain;
            }
        }
        else
        {
            $service = $this->Services->get($id);
            $this->Authorization->authorize($service, 'update');
        }

        if ($this->request->is(['patch', 'post', 'put']))
        {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            if ($service->isDirty('host_id'))
            {
                $this->Authorization->authorize($service, 'update'); // Make sure a new host_id is still valid
            }
            if ($this->Services->save($service))
            {
                $this->Flash->success(__('The service has been saved.'));

                return $this->redirect(['action' => 'view', $service->id]);
            }
            $this->Flash->error(__('The service could not be saved. Please, try again.'));
        }
        $hosts = $this->Services->Hosts->find('list', ['limit' => 200]);
        $hosts = $this->Authorization->applyScope($hosts, 'index');
        $this->set(compact('service', 'hosts'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Service id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $service = $this->Services->get($id);

        $this->Authorization->authorize($service);

        if ($this->Services->delete($service))
        {
            $this->Flash->success(__('The service has been deleted.'));
        }
        else
        {
            $this->Flash->error(__('The service could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Hosts', 'action' => 'view', $service->host_id]);
    }

    /**
     * Perform service and show results
     *
     * Ping is not logged!
     *
     * @param string|null $id
     */
    public function test($id = null)
    {
        $service = $this->Services->get($id);

        $this->Authorization->authorize($service, 'view');

        $runner = $this->Services->createRunner($service);

        $success = $runner->run();

        $info = $runner->getResponse();

        $this->set(compact('success', 'info'));
    }

    /**
     * Perform service and log results
     *
     * @param string|null $id
     */
    public function ping($id = null)
    {
        $this->request->allowMethod(['post', 'put', 'patch']);

        $service = $this->Services->get($id);

        $this->Authorization->authorize($service, 'view');

        $runner = $this->Services->createRunner($service);

        $success = $runner->run();

        $info = $runner->getResponse();

        $ping = $this->Services->Pings->savePingResult($service, $success, $runner);

        $this->set(compact('success', 'info', 'ping'));
    }

}
