<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Emails Controller
 *
 * @property \App\Model\Table\EmailsTable $Emails
 * @method \App\Model\Entity\Email[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmailsController extends AppController
{

    /**
     * Add new email address
     *
     * @param int|string|null $service_id
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($service_id = null)
    {
        $service = $this->Emails->Services->get($service_id);

        $this->Authorization->authorize($service, 'create');

        $email = $this->Emails->newEmptyEntity();

        if ($this->request->is('post'))
        {
            $email = $this->Emails->patchEntity($email, $this->request->getData());
            $email->service_id = $service->id;

            if ($this->Emails->save($email))
            {
                $this->Flash->success(__('New email has been added'));

                return $this->redirect(['controller' => 'Services', 'action' => 'view', $service->id]);
            }
            $this->Flash->error(__('The email could not be saved'));
        }
        $this->set(compact('email', 'service'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Email id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $email = $this->Emails->get($id);

        $this->Authorization->authorize($email);

        if ($this->Emails->delete($email))
        {
            $this->Flash->success(__('Extra email has been deleted'));
        }
        else
        {
            $this->Flash->error(__('The extra email could not be deleted'));
        }

        return $this->redirect(['controller' => 'Services', 'action' => 'view', $email->service_id]);
    }

}
