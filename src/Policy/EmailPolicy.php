<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\Email;
use Authorization\IdentityInterface;
use Cake\ORM\TableRegistry;

/**
 * Email policy
 */
class EmailPolicy
{
    /**
     * Check if $user can add Email
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Email $email
     * @return bool
     */
    public function canCreate(IdentityInterface $user, Email $email)
    {
        return $this->isOwnedBy($user, $email);
    }

    /**
     * Check if $user can delete Email
     *
     * @param \Authorization\IdentityInterface $user The user.
     * @param \App\Model\Entity\Email $email
     * @return bool
     */
    public function canDelete(IdentityInterface $user, Email $email)
    {
        return $this->canCreate($user, $email);
    }

    /**
     * Check if user owns the host that belongs to this service
     *
     * @param IdentityInterface $user
     * @param \App\Model\Entity\Email $email
     * @return bool
     */
    private function isOwnedBy(IdentityInterface $user, Email $email)
    {
        $hostsTable = TableRegistry::getTableLocator()->get('Hosts');
        $host = $hostsTable->find()
            ->innerJoinWith('Services')
            ->where(['Services.id' => $email->service_id])
            ->first();

        return $host->user_id === $user->id;
    }
}
