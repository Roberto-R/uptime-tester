<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\User;
use Authorization\IdentityInterface;
use App\Model\Table\UsersTable;

/**
 * User policy
 */
class UserPolicy
{

    /**
     * Check if $user can update User
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\User $resource
     * @return bool
     */
    public function canUpdate(IdentityInterface $user, User $resource)
    {
        if ($user->getOriginalData()['type'] == UsersTable::TYPE_ADMIN)
        {
            return true;
        }
        return $this->isUser($user, $resource);
    }

    /**
     * Check if $user can delete User
     *
     * @param Authorization\IdentityInterface $user The user.
     * @param App\Model\Entity\User $resource
     * @return bool
     */
    public function canDelete(IdentityInterface $user, User $resource)
    {
        return $this->canUpdate($user, $resource);
    }
    
    /**
     * Check if the modified user is the currently logged in user
     * 
     * @param IdentityInterface $user
     * @param User $resource
     * @return bool
     */
    private function isUser(IdentityInterface $user, User $resource)
    {
        return $user->id === $resource->id;
    }
}
