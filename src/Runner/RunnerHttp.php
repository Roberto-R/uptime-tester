<?php

declare(strict_types=1);

namespace App\Runner;

use Cake\Http\Client;
use Cake\Http\Client\Exception\RequestException;

/**
 * HTTP(S) service
 */
class RunnerHttp extends Runner
{

    /**
     * Run HTTP test
     *
     * Currently, only a 2xx code is registered as successful.
     *
     * @return bool
     */
    protected function _run() : bool
    {
        $http = new Client();

        try
        {
            $response = $http->get($this->service->target);
            $this->response = (string)$response->getBody()->getContents();
            $code = $response->getStatusCode();
        }
        catch (RequestException $e)
        {
            $this->response = $e->getMessage();
            return false;
        }

        return ($code >= 200 && $code < 300);
    }

}
